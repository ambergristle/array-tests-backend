### Hiring - Backend Test 1

We build RESTful API's to deliver financial and credit services to our clients.

Due to our business's nature, we maintain a PCI Level 1 Service Provider environment to transmit, store and retrieve sensitive data. Our engineers go through yearly security training, and our infrastructure and code go through regular security audits.

The first step in building a new consumer-facing web application is the successful storage of authentication credentials. Next, we would like you to create an API that allows users to sign up for an imaginary website and log in securely.

##### The objectives are:

- Create a web application in a folder called `<lastname>-backend-test-1`.
- Build the necessary REST endpoints that allow users to sign up with an email address and password, log in with an existing email address and password, and log out.
- Design a database schema for your preferred database engine to store and query the credentials. Make sure to include the SQL table creation scripts in your repository.
- Provide a Postman collection or sufficient documentation for testing the routes and validating the functionality.
- Create a `Dockerfile` and `docker-compose.yml` that allows us to build and start your project via `docker-compose up -d'. 

##### When complete:

Please write a `README` that includes how to start your application and anything we should know. Please include notes on any design choices you made throughout the exercise (dependencies, libraries, assumptions, etc.).

Submit your test as a zip file to: https://docs.google.com/forms/d/e/1FAIpQLSfWYuJ4T1OZ1Bmc77fMJCe3X21AzOKhfdvAobWzupSeR3U7CA/viewform

We will review your submission within two business days!