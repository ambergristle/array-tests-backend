/*
  handle standard errors by returning response with appropriate error code and
  message, logging and returning generic error otherwise
  @param err error object
  @param req request object (unused)
  @param res response object
  @param next handler (unused)
  @returns error response with descriptive message (if available)
*/
const handleErrors = (err, _req, res, _next) => {
  if (err.statusCode && err.message) {
    return res.status(err.statusCode).json({ message: err.message });
  }

  console.error(err.message);

  return res.status(500).json({ message: "something went wrong" });
};

module.exports = handleErrors;
