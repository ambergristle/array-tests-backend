// define query format string and passed values for each query type
const models = {
  getUserIdByEmail: (email) => ({
    queryString: "SELECT id FROM users WHERE email = $1",
    values: [email],
  }),
  getUserByEmail: (email) => ({
    queryString: "SELECT id, password FROM users WHERE email = $1",
    values: [email],
  }),
  addUserCreds: ({ id, email, password }) => ({
    queryString:
      "INSERT INTO users(id, email, password) VALUES($1, $2, $3) RETURNING *",
    values: [id, email, password],
  }),
  createSession: ({ id, userId, sessionStart, sessionEnd }) => ({
    queryString:
      "INSERT INTO sessions(id, user_id, session_start, session_end) VALUES($1, $2, $3, $4) RETURNING *",
    values: [id, userId, sessionStart, sessionEnd],
  }),
  deleteSession: (id) => ({
    queryString: "DELETE FROM sessions WHERE user_id = $1",
    values: [id],
  }),
};

module.exports = models;
