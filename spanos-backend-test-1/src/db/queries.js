const db = require("./index");

/*
  check if user exists in db with provided email
  @param email valid user email
  @returns boolean indicating insertion success
*/
const emailIsInUse = async (email) => {
  const result = await db.query("getUserIdByEmail", email);
  return result.length > 0;
};

/*
  add user to db
  @param user { id: uuid, email: string, password: hash }
  @returns uuid corresponding to user
*/
const registerUser = async ({ id, email, password }) => {
  const result = await db.query("addUserCreds", { id, email, password });
  return result[0].id;
};

/*
  get user id and password by email for auth
  @param email valid user email
  @returns user object (first/only row returned)
*/
const getUserByEmail = async (email) => {
  const result = await db.query("getUserByEmail", email);
  return result[0];
};

/*
  store new session info in db
  @param session { id: uuid, userId: uuid, sessionStart: dt, sessionEnd: dt }
  @returns session object
*/
const saveSession = async ({ id, userId, sessionStart, sessionEnd }) => {
  return await db.query("createSession", {
    id,
    userId,
    sessionStart,
    sessionEnd,
  })[0];
};

/*
  remove user's session info from db, invalidating refresh token
  @param id user uuid
  @returns session object
*/
const endSession = async (id) => {
  return await db.query("deleteSession", id);
};

module.exports = {
  emailIsInUse,
  registerUser,
  getUserByEmail,
  saveSession,
  endSession,
};
