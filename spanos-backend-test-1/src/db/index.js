const { Pool } = require("pg");
const models = require("./models");
const { ServerError } = require("../utils/errors");

// creates a new pool using env vars
const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
  ssl: process.env.NODE_ENV === "production",
});

/*
  creates a self-closing pool connection and executes passed query
  @param queryType string (models key)
  @param queryValues string | string{} (values passed to query)
  @returns query result rows ([] if none)
*/
const query = async (queryType, queryValues) => {
  const { queryString, values } = models[queryType](queryValues);

  try {
    const result = await pool.query(queryString, values);
    return result.rows;
  } catch (err) {
    throw new ServerError(500, "query failed");
  }
};

module.exports = { query };
