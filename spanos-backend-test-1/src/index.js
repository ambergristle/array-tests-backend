const express = require("express");
const cors = require("cors");
const handleErrors = require("./middleware/handleErrors");
const users = require("./routes/users");
const { RequestError } = require("./utils/errors");

const app = express();

// middleware
app.use(cors({ origin: process.env.DOMAIN }));
app.use(express.json());

// routes
app.use("/users", users);

// catch invalid routes
app.all("*", (_req, res) => {
  throw new RequestError(404, "resource not found");
});

// error handling
app.use(handleErrors);

// listen for requests at port specified in env or default
const port = process.env.PORT || 8000;

app.listen(port, function () {
  console.log(`listening on port ${port}`);
});
