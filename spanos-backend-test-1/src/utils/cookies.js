const { serialize } = require("cookie");

/*
  restrict access token cookie to sameSite and httpOnly, sets expiration
  @param minutes number of minutes before cookie expiration
  @returns cookie options object
*/
const cookieOptions = (minutes) => ({
  domain: process.env.DOMAIN,
  maxAge: minutes * 60,
  httpOnly: true,
  sameSite: true,
});

/*
  create access token cookie with passed value and expiration
  @param value string access jtw
  @param maxAge number of minutes before cookie expiration
  @returns cookie object to be returned with response
*/
const setCookie = (value, maxAge) =>
  serialize("aId", value, cookieOptions(maxAge));

module.exports = { setCookie };
