const { v4: uuid } = require("uuid");
const { saveSession } = require("../db/queries");
const { signTokens } = require("./tokens");

/*
  generate new user object with uuid
  @param user creds { email, password }
  @returns user object { id, email, password }
*/
const newUser = ({ email, password }) => ({
  id: uuid(),
  email,
  password,
});

/*
  generate new session object with uuid and start/end dt
  @param userId user uuid
  @returns session object { id, userId, sessionStart, sessionEnd }
*/
const newSession = (userId) => {
  const now = new Date();
  return {
    id: uuid(),
    userId,
    sessionStart: now,
    sessionEnd: new Date(now.getTime() + 60 * 60 * 1000),
  };
};

/*
  create a new session for user and sign tokens
  @param userId user uuid
  @returns signed jwts { accessToken, refreshToken }
*/
const createNewSession = async (userId) => {
  const session = newSession(userId);

  await saveSession(session);

  return signTokens(session);
};

module.exports = { newUser, createNewSession };
