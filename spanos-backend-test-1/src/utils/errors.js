/*
  add statusCode to generic Error class; used as base for specialized
  @param statuScode number (http status code)
  @param message string (custom error msg)
  @returns named RequestError instance
*/
class AppError extends Error {
  constructor(statusCode, message) {
    super(message);

    this.statusCode = statusCode;
  }
}

/*
  thrown for all request errors
  @param statuScode number (http status code)
  @param message string (custom error msg)
  @returns named RequestError instance
*/
class RequestError extends AppError {
  constructor(statusCode, message) {
    super(statusCode, message);

    this.name = "RequestError";
  }
}

/*
  thrown for all server-side errors
  @param statuScode number (http status code)
  @param message string (custom error msg)
  @returns named ServerError instance
*/
class ServerError extends AppError {
  constructor(statusCode, message) {
    super(statusCode, message);

    this.name = "ServerError";
  }
}

module.exports = { RequestError, ServerError };
