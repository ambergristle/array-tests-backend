const { sign, verify } = require("jsonwebtoken");
const { RequestError } = require("./errors");

const { SECRET_ACCESS_KEY, SECRET_REFRESH_KEY } = process.env;

/*
  sign access and refresh tokens containing session data with secret keys
  @param session object { id, userId, sessionStart, sessionEnd }
  @returns object { accessToken, refreshToken }
*/
const signTokens = (session) => ({
  accessToken: sign(session, SECRET_ACCESS_KEY, { expiresIn: "15m" }),
  refreshToken: sign(session, SECRET_REFRESH_KEY, { expiresIn: "60m" }),
});

/*
  decode valid (even if expired) refresh jwts
  @param refreshToken jwt
  @returns decoded session data
*/
const decodeValidToken = (refreshToken) =>
  verify(
    refreshToken,
    SECRET_REFRESH_KEY,
    { ignoreExpiration: true },
    (err, payload) => {
      if (err) throw new RequestError(403);
      return payload;
    }
  );

module.exports = { signTokens, decodeValidToken };
