const yup = require("yup");
const { RequestError } = require("./errors");

// helper function abstracts template string interpolation
const mustInclude = (charType) =>
  `password must include at least two ${charType}`;

const refreshSchema = yup.object().shape({
  rId: yup.string().required("refresh token required"),
});

// email value must be string and respect email address format
// 50 char constraint added due to col type
const emailSchema = yup.object().shape({
  email: yup
    .string()
    .required("email is required")
    .max(50, "email must be less than or equal to 50 chars")
    .email("email must be valid"),
});

// extends email schema with required password
// password value must be 10 chars, including 2 (upper, lower, digits, symbols)
const userSchema = emailSchema.shape({
  password: yup
    .string()
    .required("password is required")
    .min(10, "password must be at least 10 characters")
    .matches(/[A-Z].*[A-Z]/, mustInclude("upper-case characters"))
    .matches(/[a-z].*[a-z]/, mustInclude("lower-case characters"))
    .matches(/[0-9].*[0-9]/, mustInclude("digits"))
    .matches(/[\W].*[\W]/, mustInclude("symbols")),
});

/*
  checks passed value(s) against relevant schema(s)
  runs all validation, concatenating errors (if any)
  @param schema yup validation schema
  @param values values(s) to be validated
  @returns bool or throws error
*/
const validatePayload = async (values, schema) => {
  try {
    return await schema.validate(values, { abortEarly: false });
  } catch (err) {
    throw new RequestError(400, err.errors);
  }
};

module.exports = { refreshSchema, userSchema, validatePayload };
