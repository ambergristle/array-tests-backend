const {
  refreshSchema,
  validatePayload,
} = require("../../utils/validationSchema");
const { decodeValidToken } = require("../../utils/tokens");
const { endSession } = require("../../db/queries");
const { setCookie } = require("../../utils/cookies");
const { ServerError } = require("../../utils/errors");

/*
  end user session when provided with valid refresh token
  @param req request object
  @param res response object
  @param next (error) handler
  @returns ok response clearing access token cookie
*/
const logout = async (req, res, next) => {
  try {
    // verify that email and password are in body and of valid shape
    const payload = await validatePayload(req.body, refreshSchema);

    // verify and decode refresh token
    const { userId } = await decodeValidToken(payload.rId);

    // delete session
    const sessionDeleted = await endSession(userId);

    if (!sessionDeleted) throw new ServerError(500, "logout failed");

    // invalidate tokens
    res.setHeader("Set-Cookie", setCookie(null, 0));

    return res.sendStatus(200);
  } catch (err) {
    next(err); // pass error to handler
  }
};

module.exports = logout;
