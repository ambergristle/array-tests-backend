const { compare } = require("bcrypt");
const { userSchema, validatePayload } = require("../../utils/validationSchema");
const { getUserByEmail } = require("../../db/queries");
const { setCookie } = require("../../utils/cookies");
const { createNewSession } = require("../../utils/sessions");
const { RequestError } = require("../../utils/errors");

/*
  authenticate and authorize users with email and password, creating a new
  session with 30 min lifetime, verified and persisted with jwts
  @param req request object
  @param res response object
  @param next (error) handler
  @returns ok response with access and refresh tokens
*/
const login = async (req, res, next) => {
  try {
    // verify that email and password are in body and of valid shape
    const { email, password } = await validatePayload(req.body, userSchema);

    // query users by email
    const user = await getUserByEmail(email);
    if (!user) throw new RequestError(404, "user not found");

    const { id, password: hash } = user;

    // check password
    const passwordIsValid = await compare(password, hash);
    if (!passwordIsValid) throw new RequestError(401, "invalid password");

    // create session, set access and refresh tokens
    const { accessToken, refreshToken } = await createNewSession(id);

    // return access token in http-only cookie
    res.setHeader("Set-Cookie", setCookie(accessToken, 15));

    // return refresh token in body
    return res
      .status(200)
      .send({ message: "successfully logged in", rId: refreshToken });
  } catch (err) {
    next(err); // pass error to handler
  }
};

module.exports = login;
