const { hash } = require("bcrypt");
const { userSchema, validatePayload } = require("../../utils/validationSchema");
const { emailIsInUse, registerUser } = require("../../db/queries");
const { setCookie } = require("../../utils/cookies");
const { newUser, createNewSession } = require("../../utils/sessions");
const { RequestError, ServerError } = require("../../utils/errors");

/*
  register a new user with unique email and valid password, storing user info
  (with password hashed) in db, and creating a new session with 30 min lifetime,
  verified and persisted with jwts
  @param req request object
  @param res response object
  @param next (error) handler
  @returns ok response with access and refresh tokens
*/
const signup = async (req, res, next) => {
  try {
    // verify that email and password are in body and of valid shape
    const { email, password } = await validatePayload(req.body, userSchema);

    // check if email is in use
    if (await emailIsInUse(email)) throw new RequestError(409, "email in use");

    // hash password
    const hashedPassword = await hash(password, 10);

    if (!hashedPassword)
      throw new ServerError(500, "could not securely store new user");

    // create user object with uuid for storage, session creation
    const user = newUser({ email, password: hashedPassword });

    // add user to table
    const userId = await registerUser(user);

    // create session, set access and refresh tokens
    const { accessToken, refreshToken } = await createNewSession(userId);

    // return access token in http-only cookie
    res.setHeader("Set-Cookie", setCookie(accessToken, 15));

    // return refresh token in body
    return res
      .status(201)
      .json({ message: `welcome ${email}`, rId: refreshToken });
  } catch (err) {
    next(err); // pass error to handler
  }
};

module.exports = signup;
