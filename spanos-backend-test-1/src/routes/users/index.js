const express = require("express");
const handleSignup = require("./signup");
const handleLogin = require("./login");
const handleLogout = require("./logout");
const { RequestError } = require("../../utils/errors");

const router = express.Router();

// define valid routes for use in catch-all handler
const validRoutes = ["/signup", "/login", "/logout"];

router.post("/signup", handleSignup);
router.post("/login", handleLogin);
router.delete("/logout", handleLogout);

// catch invalid methods/routes
router.all("*", (req) => {
  const validPath = validRoutes.includes(req.path);

  if (!validPath) {
    throw new RequestError(404, "resource not found");
  } else {
    throw new RequestError(405, "invalid method");
  }
});

module.exports = router;
