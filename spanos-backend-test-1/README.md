### Instructions

1. ensure Docker and Docker Compose are installed
2. clone the repository
3. `cd` into `spanos-backend-test-1`
4. run `docker compose up`
5. sign up by sending a `post` request to `/users/signup`
   - include `{ email: string, password: string }` in the request body
     - email must be <= 50 characters and in valid email format (e.g, example@test.com)
     - password must be >= 10 characters and <=60 characters
       - it must include 2 each, uppercase, lowercase, digits, symbols
   - if valid values are sent, you will be logged in and receive a refresh token (`rId`)
6. log out by sending a `delete` request to `/users/logout`
   - include `{ rId: refreshToken }` in the request body
     - use the refresh token value returned by your signup/login request
7. log back in by sending a `post` request to `/users/login`
   - include `{ email: yourEmail, password: yourPassword }` in the request body

You can use the Hoppscotch collection located here (https://hopp.sh/r/PzdNIIb4Cyle) to make the requests. The collection request bodies have been pre-populated, though you will need to update the refresh token (`rId`) in the body of `/logout` requests with the token returned by your `/signup` or login request. _Note that refresh tokens expire in 60 minutes._

### Design

Using the Express library, I created three endpoints, one for each operation (signup, login, and logout), nested within a "users" route. While not strictly necessary given this assignment's scope, breaking my code into smaller pieces made it easier to design, write, and test.

The handlers for each endpoint communicated as needed with a Postgres database initialized and connected to with Pg. I abstracted these queries into a customized query function and templated query parameters stored in an object ("models"), making my codebase clearer and more concise.

#### /signup

To sign up, a `post` request must be sent with a valid email and password stored in the request body. I wrote middleware to verify that the required keys are present and their values are valid. While I only checked email address shape (string with shape "example@domain.tld"), more comprehensive validation could easily be added to this function. Passwords were checked for length and the inclusion of digits and symbols, mandating a minimum of security.

Once the request body is validated, the handler checks the provided email against existing users, to confirm it is still available. The email and password (hashed using Bcrypt) are then added to a Users table, or an error is thrown (if there is an email conflict). Hashing stored passwords is a simple step that makes their misuse difficult in the event of a system breach.

#### /login

To log in, a `post` request must be sent with a valid email and password stored in the request body. I added this check principally to reduce invalid calls to the database, and to reduce response time to user input errors.

Valid requests are passed to the route handler, which queries the Users table for the email. If found, the associated password hash is returned, and compared to the provided password using Bcrypt. Otherwise, an error is thrown, indicating a user with that email does not exists.

If the email is valid but the password does not match, an error is thrown, indicating the user cannot be authenticated. Otherwise, access and refresh tokens are created using JsonWebToken to represent an active user session. JWTs are tamper-resistant vehicles for user and session information, and can be used to verify user authorization both client and server-side.

_Using an access/refresh token pair allows for more secure extended sessions. A single lengthy (or open-ended) session is vulnerable to token theft and to multi-user devices. Short-living access tokens mitigate these issues, and a longer-living refresh token maintains session continuity, removing the need for regular re-authentication._

The refresh token is then saved in the Sessions table, allowing admins (and users, via logout) to invalidate/end a token/session, and the tokens are returned in the response. An `http-only` cookie is used to return the access token, mitigating XSS attacks, while the refresh token is returned in the response body so it can be accessed by client-side scripts (for in-app auth or data transfer, e.g., user info).

#### /logout

To logout, users must simply send a `delete` request with a valid refresh token stored in the request body. The token is then decoded, to authenticate and authorize the user without making an additional database call. The user ID is used to query for an active session, which is then deleted, ending the session and preventing further use of the refresh token. Finally, the access token is invalidated by resetting its expiration and clearing its value.

#### Token Validation and Reissue

Though handling request validation and token reissue seemed out off scope for this assignment, I have included a brief outline of how I have handled this functionality in the past:

Auth-requiring requests will include the access token cookie, providing quick authentication and authorization. The JWT simply needs to be parsed and validated. It can also be decoded to identify user roles and other parameters, though these were out of scope.

The authentication token's expiration status can be checked client- or server-side. In either case, the token's validity (less expiration) can be checked to authenticate and authorize the request. Expired access tokens require the client to send the refresh token, which is itself validated (and must not be expired) in order for a new access and refresh token to be issued (the latter of which overwrites existing session information in the database).

#### Invalid Endpoints and Methods

If a request is not caught and handled by any existing endpoints, it is passed to a catch-all that throws an error, passing more specific information about the issue to the custom error handler.

#### Error Handling

Custom error-handling middleware catches any thrown errors and passes the specified http status code and message back to the client in the response. Unanticipated server-side errors will return a 500 and a generic status code.

#### Docker

I created a Dockerfile for both the database and the API, referenced by a docker-compose file specifying environment variables and other parameters. Though this is deeply insecure, it seemed acceptable in this context. As the backend is dependent on the database, I listed it second, giving Docker some time to get the database running, though in a production context it would be wise to support connection retries. The app build process begins by loading dependencies, followed by copying and running source code, facilitating process cacheing.

### Dependencies

#### bcrypt

Bcrypt provides a suite of straightforward hashing and hash validation methods.

#### cors

Cors offers simple request origin validation for Express applications, blocking invalid clients from accessing server resources.

#### express

Express provides a modular and effective API for request handling, including its built-in error middleware and the `json` method for JSON parsing.

#### jsonwebtoken

JsonWebToken offers highly customizable token signing, decoding, and verification, with flexible error handling.

#### pg

Using an ORM like Sequelize or Prisma is optimal for larger/more complex applications, as they streamline a lot of the boilerplate associated with initializing, modeling, and querying a database. They also abstract generic SQL into JS methods, making for cleaner and more accessible code.

I chose to use Pg instead, as ORMs can be an over-engineered solution for small/lightweight applications like this one, and the project requirements seemed to emphasize the use of vanilla SQL.

#### uuid

A popular library for generating UUIDs, a means of ensuring unique but naive database indices and other identifiers.

#### yup

Yup simplifies validation by creating static or dynamic validation schemas against which primitives and objects can be checked. Schemas include validation methods that return a boolean or a more complex success/error object, supporting custom error messages. Schemas can also be extended, allowing for increased modularity.

### Dev Dependencies

#### dotenv

Dotenv allows scripts access to a `.env` file, facilitating early development outside of a more formalized code environment.

#### nodemon

Nodemon restarts a local server on code change, streamlining the development process.
