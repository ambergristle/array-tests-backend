-- create db if not exists // docker removes the need for this
-- (pg workaround; \gexec value returned from SELECT treated as command)
-- SELECT 'CREATE DATABASE spanos-backend-test-1;'
-- WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'spanos-backend-test-1')\gexec;

-- install uuid library if not already installed
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- create users table and rows on db init
-- store user data (email + password)
CREATE TABLE IF NOT EXISTS users (
  id uuid NOT NULL,
  email VARCHAR(50) NOT NULL,
  password VARCHAR(60) NOT NULL,
  PRIMARY KEY (id)
);

-- create sessions table and rows on db init
-- store session data (refresh token + start time)
CREATE TABLE IF NOT EXISTS sessions (
  id uuid NOT NULL,
  user_id uuid NOT NULL,
  session_start TIMESTAMPTZ NOT NULL,
  session_end TIMESTAMPTZ NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id)
    REFERENCES users(id)
);

-- set default db timezone
SET timezone = 'America/New_York';
